﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{
    bool isDragging = false;
    Vector2 pongForce;
    GameObject visualIndication;
    //public int CountBounce = 0;
    //public GoalTest Goal;



    [SerializeField]
    GameObject arrowPrefab;
    float arrowLengthDefault;

    [SerializeField]
    float thrust = 20.0f;

    private void Start()
    {
        arrowLengthDefault = arrowPrefab.GetComponent<SpriteRenderer>().sprite.bounds.size.x;
    }

    private void OnMouseUp()
    {
        if (isDragging)
        {
            GetComponent<Rigidbody2D>().AddForce(pongForce * thrust);
            pongForce = new Vector2();
            isDragging = false;
            Destroy(visualIndication);
        }
    }


    private void OnMouseDrag()
    {
        isDragging = true;

        Vector2 mousePosition = Input.mousePosition;
        Vector2 objectPosition = Camera.main.ScreenToWorldPoint(mousePosition);
        pongForce = (Vector2)transform.position - objectPosition;

        // Arrow
        //Vector2 center = objectPosition + pongForce / 2f;
        if (visualIndication) Destroy(visualIndication);
        float angle = Mathf.Atan2(pongForce.y, pongForce.x) * Mathf.Rad2Deg;
        Quaternion quaternion = Quaternion.AngleAxis(angle, Vector3.forward);
        Vector2 scale = new Vector2(pongForce.magnitude / arrowLengthDefault, 1);
        visualIndication = Instantiate(arrowPrefab, transform.position, quaternion);
        visualIndication.transform.localScale = scale;
    }

    /*private void OnTriggerEnter2D(Collider2D collision) //PUT TRIGGER ON FLOOR
    {
        CountBounce += 1;
        print("It has bounced: " + CountBounce + " times");
        if (CountBounce != 0)
        {
            if (Goal.Score == 1)
            {
                print("valid goal");
            }
        }
    }*/
}
