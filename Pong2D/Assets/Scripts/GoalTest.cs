﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class GoalTest : MonoBehaviour
{
    public static int Score = 0;
    //public BounceCounter bounceCounter;

    // Use this for initialization
    private void Start () {
        //bounceCounter = FindObjectOfType<BounceCounter>();
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Score += 1;
        print("Score: " + Score);
        if(BounceCounter.CountBounce != 0)
        {
            print("Valid goal");
        }
        else
        {
            print("Invalid goal");
        }
    }
}
